const Order = require("../models/Order");
const User = require("../models/User");

// ▓ ▓ ADD ORDER
module.exports.createOrder = (req,res) => {

	if(!req.user.isAdmin){
		return res.send("Action Forbidden - Non-Admin Allowed");
	} else
	{
		let newOrder = new Order({
			name: req.body.name,
			amount: req.body.amount,
			mobileNo: req.body.mobileNo,
		})

		newOrder.save()
			.then(order => res.send(order))
			.catch(error => res.send(error))
	}
};

// ▓ ▓ RETRIEVE ALL ORDERS
module.exports.retrieveAllOrders = (req,res) => {
/*	if(!req.user.isAdmin){
		return res.send("Action Forbidden - Non-Admin Allowed");
	} else { */
		console.log(result)
		Order.find({})
			.then(result => res.send(result))
			.catch(err => res.send(err))
	//}

};

// ▓ ▓ RETRIEVE USERS ORDERS
/*module.exports.retrieveUsersOrders = (req, res) => {

	Order.findOne({mobileNo: req.body.mobileNo})
	.then(foundUser => {

		if (foundUser === null){
			return res.send("Order does not exist!");
		} else {

			(result => res.send(result))
		}
	}
	.catch(err => res.send(err))
};
*/