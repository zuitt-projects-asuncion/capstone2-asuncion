//import product Model
const Product = require("../models/Product");
const Order = require("../models/Order");
const User = require("../models/User");


// ▓ ▓ CREATE PRODUCT
module.exports.createProduct = (req,res) => {

	console.log(req.body);

	let newProduct = new Product({

		name: req.body.name,
		brand: req.body.brand,
		ip: req.body.ip,
		description: req.body.description,
		scale: req.body.scale,
		price: req.body.price

	})

	newProduct.save()
	.then(product => res.send(product))
	.catch(error => res.send(error))
};

// ▓ ▓ RETRIEVE ALL PRODUCTS
module.exports.getAllProducts = (req,res) => {

	Product.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));

}

// ▓ ▓ RETRIEVE SINGLE PRODUCT
module.exports.retrieveSingleProduct = (req,res) => {

	Product.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err))

}


// ▓ ▓ RETRIEVE ALL ACTIVE PRODUCTS
module.exports.retrieveAllActiveProducts = (req,res) => {

	Product.find({isActive: true})
	.then(result => res.send(result))
	.catch(err => res.send(err));

};

// ▓ ▓ UPDATE PRODUCT INFORMATION
module.exports.updateProduct = (req, res) => {

	console.log(req.params.id);
	console.log(req.body);

	let updates = {

		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
		isActive: req.body.isActive
	}

	Product.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then(updatedProduct => res.send(updatedProduct))
	.catch(err => res.send(err));

}

// ▓ ▓ GET INACTIVE PRODUCT
module.exports.getInactiveProducts = (req, res) => {

	Product.find({isActive: false})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

// ▓ ▓ ACTIVATE PRODUCT
module.exports.activate = (req,res) => {
	let updates = {
		isActive: true
	}

	Product.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedProduct => res.send(updatedProduct))
	.catch(err => res.send(err));

}

// ▓ ▓ ARCHIVE PRODUCT
module.exports.archive = (req,res) => {

	let updates = {
		isActive: false
	}

	Product.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedProduct => res.send(updatedProduct))
	.catch(err => res.send(err));

}

// ▓ ▓ FIND PRODUCT BY NAME
module.exports.findProductsByName = (req, res) => {

	Product.find({name: {$regex: req.body.name, $options: '$i'}})
	.then(result => {

		if(result.length === 0){
			return res.send("No products found");
		} else {
			return res.send(result);
		}
	})
	.catch(err => res.send(err));
};

// ▓ ▓ FIND PRODUCT BY BRAND
module.exports.findProductsByBrand = (req, res) => {

	Product.find({brand: {$regex: req.body.brand, $options: '$i'}})
	.then(result => {
		console.log(result)
		if(result.length === 0){
			return res.send("No products found");
		} else {
			return res.send(result);
		}
	})
	.catch(err => res.send(err));
};

// ▓ ▓ FIND PRODUCT BY IP
module.exports.findProductsByIp = (req, res) => {

	Product.find({ip: {$regex: req.body.brand, $options: '$i'}})
	.then(result => {
		console.log(result)
		if(result.length === 0){
			return res.send("No products found");
		} else {
			return res.send(result);
		}
	})
	.catch(err => res.send(err));
};

// ▓ ▓ FIND PRODUCT BY SCALE
module.exports.findProductsByScale = (req, res) => {

	Product.find({price: req.body.scale})
	.then(result => {

		console.log(result)
		if(result.length === 0){
			return res.send("No product found");
		
		} else {
			return res.send(result);
		}
	})
	.catch(err => res.send(err));
};

// ▓ ▓ FIND PRODUCT BY PRICE
module.exports.findProductsByPrice = (req, res) => {

	Product.find({price: req.body.price})
	.then(result => {

		console.log(result)
		if(result.length === 0){
			return res.send("No product found");
		
		} else {
			return res.send(result);
		}
	})
	.catch(err => res.send(err));
};

// ▓ ▓ RETRIEVE USERS ORDERS
module.exports.getAllOrders = (req,res) => {

	if(req.user.isAdmin){
		Order.find({})
			.then(result => res.send(result))
			.catch(err => res.send(err))
	} else {
		res.send("Access Denied - Admin Only");
	}
};


// ▓ ▓ RETRIEVE authenticated USER's ORDERS 
module.exports.getCustomersOrders = (req, res) => {

	//console.log(result)
	Product.find({_id: req.body._id})
	.then(result => res.send(result))
	.catch(error => res.send(error));

};