const bcrypt = require("bcrypt");
const User = require("../models/User");
const Product = require("../models/Product");
const auth = require("../auth");

// ▓ ▓ USER REGISTRTION
module.exports.registerUser = (req, res) => {

	console.log(req.body);

	const hashedPW = bcrypt.hashSync(req.body.password, 10);

	let newUser = new User({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		mobileNo: req.body.mobileNo,
		password: hashedPW
	})

	newUser.save()
	.then(user => res.send(user))
	.catch(err => res.send(err));
};

// ▓ ▓ USER AUTHENTICATION
module.exports.loginUser = (req, res) => {

	console.log(req.body);

	User.findOne({email: req.body.email})
	.then(foundUser => {

		if(foundUser === null){

			return res.send("User does not exist");
		
		} else {

			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)

			 if(isPasswordCorrect){

			 	return res.send({accessToken: auth.createAccessToken(foundUser)})

			 } else {

			 	return res.send("Password is incorrect")
			 }
		}
	})
	.catch(err => res.send(err));
};

// ▓ ▓ SET USER AS ADMIN
module.exports.updateAdmin = (req, res) => {

	// req.user.id captures the id of the logged in admin
	console.log(req.user.id);
	// req.params.id captures the id designated in the params
	console.log(req.params.id);
	console.log(req.params.ifAdmin);

	let updates = {
		isAdmin: true
	}

	User.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err));
};

// ▓ ▓ RETRIEVE ALL USERS
module.exports.getAllUsers = (req, res) => {

	User.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

// ▓ ▓ CREATE ORDER

module.exports.createOrder = async (req, res) => {

	console.log(req.user.id);
	console.log(req.body.productId);

	if(req.user.isAdmin){
		return res.send("Action Forbidden");

	};


	let isUserUpdated = await User.findById(req.user.id).then(user => {

		console.log(user);

		let newOrder = {
			productId: req.body.productId,
		}

		user.orders.push(newOrder);

		return user.save().then(user => true).catch(err => err.message);
	})


	if(isUserUpdated !== true){
		return res.send({message: isUserUpdated})
	}

	let isProductUpdated = await Product.findById(req.body.productId).then(product => {

		console.log(product);

		let customer = {
			userId: req.user.id,
		}

		product.customers.push(customer);

		return product.save().then(product => true).catch(err => err.message)
	});

	if(isProductUpdated !== true){
		return res.send({message: isProductUpdated})
	};

	if(isUserUpdated && isProductUpdated){
		return res.send({message: 'Your order has been submitted for review.'})
	};

};

// ▓ ▓ GENERATE ORDERS REPORT || admin only
module.exports.getAllOrders = (req, res) => {

	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));
};