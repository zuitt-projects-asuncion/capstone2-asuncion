const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({

	amount: {
		type: Number,
		required: [true, "totalAmount is required."]
	},

	name: {
		type: String,
		required: [true,"Name is required."]
	},
	
	purchasedOn: {
		type: Date,
		default: new Date()	
	},

	mobileNo: {
		type: String,
		required: [true, "Mobile Number is required"]
	}

	
})

module.exports = mongoose.model("Order", orderSchema);
