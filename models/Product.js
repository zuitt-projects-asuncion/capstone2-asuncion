const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({

	name: {
		type: String,
		required: [true,"Name is required."]
	},

	brand: {
		type: String,
		required: [true, "Brand is required."]
	},

	ip: {
		type: String,
		required: [true, "I.P. is required."]
	},	

	description: {
		type: String,
		required: [true, "Description is required."]
	},

	scale: {
		type: String,
		required: [true, "Scale is required."]
	},

	price: {
		type: Number,
		required: [true, "Price is required."]
	},

	isActive: {
		type: Boolean,
		default: true
	},

	createdOn: {
		type: Date,
		default: new Date()
	},


	customers: [

		{
			userId: {
				type: String,
				required: [true, "User Id is required"]
			},

			purchasedRequestedOn: {
				type: Date,
				default: new Date()
			},
			
			status: {
				type: String,
				default: "pending"
			}
		}

	]



})

module.exports = mongoose.model("Product",productSchema);
