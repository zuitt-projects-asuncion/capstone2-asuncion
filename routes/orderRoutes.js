const express = require("express");
const router = express.Router();
const orderControllers = require("../controllers/orderControllers");

/*const productControllers = require("../controllers/productControllers");
const userControllers = require("../controllers/userControllers");*/

const auth = require("../auth");

const {verify, verifyAdmin} = auth;

// ▓ ▓ CREATE ORDER
router.post('/', verify, verifyAdmin, orderControllers.createOrder);

// ▓ ▓ RETRIEVE ALL ORDERS
router.get('/', /*verify, verifyAdmin,*/ orderControllers.retrieveAllOrders);

// ▓ ▓ RETRIEVE USER'S ORDERS
//router.get('/', verify, orderControllers.retrieveUsersOrders);


module.exports = router;