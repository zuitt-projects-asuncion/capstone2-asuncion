const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");
const orderControllers = require("../controllers/orderControllers");
const userControllers = require("../controllers/userControllers");

const auth = require("../auth");

const {verify, verifyAdmin} = auth;

//Routes

// ▓ ▓ CREATE PRODUCT
router.post('/', verify, verifyAdmin, productControllers.createProduct);

// ▓ ▓ RETRIEVE ALL PRODUCTS
router.get('/', productControllers.getAllProducts);

// ▓ ▓ RETRIEVE SINGLE PRODUCT
router.get('/retrieveSingleProduct/:id',productControllers.retrieveSingleProduct);

// ▓ ▓ RETRIEVE ALL ACTIVE PRODUCTS
router.get('/retrieveAllActiveProducts',productControllers.retrieveAllActiveProducts);

// ▓ ▓ UPDATE PRODUCT INFORMATION
router.put("/:id", verify, verifyAdmin, productControllers.updateProduct);

// ▓ ▓ GET INACTIVE PRODUCT
router.get("/getInactiveProducts", verify, verifyAdmin, productControllers.getInactiveProducts);

// ▓ ▓ ARCHIVE PRODUCT
router.put('/archive/:id',verify, verifyAdmin,productControllers.archive);

// ▓ ▓ ACTIVATE PRODUCT
router.put('/activate/:id',verify, verifyAdmin,productControllers.activate);

// ▓ ▓ FIND PRODUCTS BY NAME
router.post("/findProductsByName", productControllers.findProductsByName);

// ▓ ▓ FIND PRODUCTS BY BRAND
router.post("/findProductsByBrand", productControllers.findProductsByBrand);

// ▓ ▓ FIND PRODUCTS BY IP
router.post("/findProductsByIp", productControllers.findProductsByIp);

// ▓ ▓ FIND PRODUCTS BY IP
router.post("/findProductsByIp", productControllers.findProductsByIp);

// ▓ ▓ FIND PRODUCTS BY SCALE
router.post("/findProductsByScale", productControllers.findProductsByScale);

// ▓ ▓ FIND PRODUCT BY PRICE
router.post("/findProductsByPrice", productControllers.findProductsByPrice);

// ▓ ▓ RETRIEVE ALL ORDERS
/*router.get('/', verify, verifyAdmin, orderControllers.getAllOrders);*/

// ▓ ▓ RETRIEVE authenticated USER's ORDERS
router.get("/getCustomersOrders/:id", verify, productControllers.getCustomersOrders);

module.exports = router;