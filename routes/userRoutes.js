const express = require('express');
const router = express.Router();

const userControllers = require("../controllers/userControllers");

const auth = require("../auth");

const {verify, verifyAdmin} = auth;

// ▓ ▓ USER REGISTRTION
router.post("/", userControllers.registerUser);

// ▓ ▓ USER AUTHENTICATION
router.post("/login", userControllers.loginUser);

// ▓ ▓ SET USER AS ADMIN
router.put("/updateAdmin/:id", verify, verifyAdmin, userControllers.updateAdmin);

// ▓ ▓ RETRIEVE ALL USERS
router.get("/", userControllers.getAllUsers);

// ▓ ▓ CREATE ORDER
//router.post("/createOrder", verify, userControllers.createOrder);

// ▓ ▓ CREATE ORDER
router.post("/createOrder", verify, userControllers.createOrder);

// ▓ ▓ GENERATE ORDERS REPORT || admin only
router.get("/getAllOrders", userControllers.getAllOrders);

module.exports = router;

